package com.example.docusignproject;

import com.docusign.esign.client.ApiException;
import com.example.docusignproject.docusign_auth.DocuSignAuthService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    DocuSignAuthService authService;
    @Value("${docusign.clientId}")
    public   String CLIENT_ID="";
    @Value("${docusign.concent}")
    private String concent;
    @Value("${docusign.callBackUrl}")
    private String callbackUrl;
    @Value("${docusign.authUrl}")
    public  String AUTH_SERVER ="";
    public DevBootstrap(DocuSignAuthService authService) {
        this.authService = authService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        try {
            authService.checkToken();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ApiException e) {
           // e.printStackTrace();
            System.err.println("\nDocuSign Exception!");

            // Special handling for consent_required
            String message = e.getMessage();
            if(message != null){
                String consent_url = String.format("%s/oauth/auth?response_type=code&scope=%s" +
                                "&client_id=%s" +
                                "&redirect_uri=%s",
                        AUTH_SERVER,
                        concent,
                        CLIENT_ID,
                        callbackUrl);
                System.err.println("\nC O N S E N T   R E Q U I R E D" +
                        "\nAsk the user who will be impersonated to run the following url: " +
                        "\n"+ consent_url+
                        "\n\nIt will ask the user to login and to approve access by your application." +
                        "\nAlternatively, an Administrator can use Organization Administration to" +
                        "\npre-approve one or more users.");
            } else {
                System.err.println(String.format("    Reason: %d", e.getCode()));
                System.err.println(String.format("    Error Reponse: %s", e.getResponseBody()));
            }
        }
    }
}
