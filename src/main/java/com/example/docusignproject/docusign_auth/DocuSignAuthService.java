package com.example.docusignproject.docusign_auth;

import com.docusign.esign.client.ApiClient;
import com.docusign.esign.client.ApiException;
import com.docusign.esign.client.auth.OAuth;
import com.example.docusignproject.util.ResourceUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
@Service
public class DocuSignAuthService {
    @Value("${docusign.accountId}")
    public   String ACCOUNT_ID ="";
    @Value("${docusign.clientId}")
    public   String CLIENT_ID="";
    @Value("${docusign.userId}")
    public   String USER_ID="" ;
    @Value("${docusign.baseFilePath}")
    public String basePath;
    @Value("${docusign.authUrl}")
    public  String AUTH_SERVER ="";
    @Value("${docusign.privateKeyFileName}")
    String privateKyFileName="";
    private static final long TOKEN_EXPIRATION_IN_SECONDS = 3600;
    private static final long TOKEN_REPLACEMENT_IN_MILLISECONDS = 10 * 60 * 1000;

    private static OAuth.Account _account;
    private static File privateKeyTempFile = null;
    private static long expiresIn;
    private static String _token = null;
    protected final ApiClient apiClient;

    protected static String getAccountId() {
        return _account.getAccountId();
    };


    public DocuSignAuthService(ApiClient apiClient) throws IOException {
        this.apiClient =  apiClient;
    }

    public ApiClient checkToken() throws IOException, ApiException {
        if(this._token == null
                || (System.currentTimeMillis() + TOKEN_REPLACEMENT_IN_MILLISECONDS) > this.expiresIn) {
           return updateToken();
        }
        return apiClient;
    }

    private ApiClient updateToken() throws IOException, ApiException {
        System.out.println("\nFetching an access token via JWT grant...");

        java.util.List<String> scopes = new ArrayList<String>();
        // Only signature scope is needed. Impersonation scope is implied.
        scopes.add(OAuth.Scope_SIGNATURE);
        //Path path = Paths.get(basePath, "private1.PEM");
//        String privateKey =  new String(Files.readAllBytes(path));
        //String privateKey = fileData.replace("\\n","\n");
        //byte[] privateKeyBytes = privateKey.getBytes();
        byte[] privateKeyBytes = ResourceUtils.readFile(privateKyFileName);
        apiClient.setOAuthBasePath(AUTH_SERVER);
        apiClient.setDebugging(true);
        OAuth.OAuthToken oAuthToken = apiClient.requestJWTUserToken (
                CLIENT_ID,
                USER_ID,
                scopes,
                privateKeyBytes,
                TOKEN_EXPIRATION_IN_SECONDS);
        apiClient.setAccessToken(oAuthToken.getAccessToken(), oAuthToken.getExpiresIn());
        System.out.println("Done. Continuing...\n");

//        if(_account == null)
//            _account = this.getAccountInfo(apiClient);
        // default or configured account id.
        apiClient.setBasePath(AUTH_SERVER + "/restapi");
        apiClient.addDefaultHeader("Authorization", "Bearer " + apiClient.getAccessToken());

        _token = apiClient.getAccessToken();
        expiresIn = System.currentTimeMillis() + (oAuthToken.getExpiresIn() * 1000);
        return apiClient;
    }

    private OAuth.Account getAccountInfo(ApiClient client) throws ApiException {
        OAuth.UserInfo userInfo = client.getUserInfo(client.getAccessToken());
        OAuth.Account accountInfo = null;

        if(ACCOUNT_ID == null || ACCOUNT_ID.length() == 0){
            List<OAuth.Account> accounts = userInfo.getAccounts();

            OAuth.Account acct = this.find(accounts, new ICondition<OAuth.Account>() {
                public boolean test(OAuth.Account member) {
                    return (member.getIsDefault() == "true");
                }
            });

            if (acct != null) return acct;

            acct = this.find(accounts, new ICondition<OAuth.Account>() {
                public boolean test(OAuth.Account member) {
                    return (member.getAccountId() == ACCOUNT_ID);
                }
            });

            if (acct != null) return acct;

        }

        return accountInfo;
    }

    private OAuth.Account find(List<OAuth.Account> accounts, ICondition<OAuth.Account> criteria) {
        for (OAuth.Account acct: accounts) {
            if(criteria.test(acct)){
                return acct;
            }
        }
        return null;
    }

    interface ICondition<T> {
        boolean test(T member);
    }

    public ApiClient getApiClient() {
        return apiClient;
    }
}
