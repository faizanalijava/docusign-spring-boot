package com.example.docusignproject.controller;

import com.docusign.esign.api.EnvelopesApi;
import com.docusign.esign.client.ApiClient;
import com.docusign.esign.client.ApiException;
import com.docusign.esign.model.*;
import com.example.docusignproject.DTO.ApiResponse;
import com.example.docusignproject.docusign_auth.DocuSignAuthService;
import com.example.docusignproject.util.ResourceUtils;
import com.sun.jersey.core.util.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

@RestController
@CrossOrigin
public class HomeController {
    //@Value("${docusign.accountId}")
    String accountId = "53c5951e-eb4f-4a34-ae79-19dc08f8980f";

    // Recipient Information
    String signerName = "Faizan";
    String signerEmail = "ranafaizan.cr7@gmail.com";


    // The url for this web application
    @Value("${docusign.webAppUrl}")
    String baseUrl = "";
    String clientUserId = "123"; // Used to indicate that the signer will use an embedded
    // Signing Ceremony. Represents the signer's userId within
    // your application.
    String authenticationMethod = "None"; // How is this application authenticating
    // the signer? See the `authenticationMethod' definition
    //  https://developers.docusign.com/esign-rest-api/reference/Envelopes/EnvelopeViews/createRecipient
    //
    // The API base path
    @Value("${docusign.restUrl}")
    String basePath = "";
    // The document to be signed. See /qs-java/src/main/resources/World_Wide_Corp_lorem.pdf
    @Value("${docusign.docName}")
    String docPdf = " ";

    DocuSignAuthService authService;

    public HomeController(DocuSignAuthService authService) {
        this.authService = authService;
    }

    @GetMapping("get-document")
    public ApiResponse getUrl() throws IOException, ApiException {

        ApiClient apiClient = authService.checkToken();
        apiClient.setBasePath(basePath);
        byte[] buffer = ResourceUtils.readFile(docPdf);
        String docBase64 = new String(Base64.encode(buffer));

        // Create the DocuSign document object
        Document document = new Document();
        document.setDocumentBase64(docBase64);
        document.setName("Example document"); // can be different from actual file name
        document.setFileExtension("pdf"); // many different document types are accepted
        document.setDocumentId("1"); // a label used to reference the doc

        // The signer object
        // Create a signer recipient to sign the document, identified by name and email
        // We set the clientUserId to enable embedded signing for the recipient
        Signer signer = new Signer();
        signer.setEmail(signerEmail);
        signer.setName(signerName);
        signer.clientUserId(clientUserId);
        signer.recipientId("1");

        // Create a signHere tabs (also known as a field) on the document,
        // We're using x/y positioning. Anchor string positioning can also be used
        SignHere signHere = new SignHere();
        signHere.setDocumentId("1");
        signHere.setPageNumber("1");
        signHere.setRecipientId("1");
        signHere.setTabLabel("SignHereTab");
        signHere.setXPosition("232");
        signHere.setYPosition("638");

        // Add the tabs to the signer object
        // The Tabs object wants arrays of the different field/tab types
        Tabs signerTabs = new Tabs();
        signerTabs.setSignHereTabs(Arrays.asList(signHere));
        signer.setTabs(signerTabs);

        // Next, create the top level envelope definition and populate it.
        EnvelopeDefinition envelopeDefinition = new EnvelopeDefinition();
        envelopeDefinition.setEmailSubject("Please sign this document");
        envelopeDefinition.setDocuments(Arrays.asList(document));
        // Add the recipient to the envelope object
        Recipients recipients = new Recipients();
        recipients.setSigners(Arrays.asList(signer));
        envelopeDefinition.setRecipients(recipients);
        envelopeDefinition.setStatus("sent");
        // Step 2. Call DocuSign to create and send the envelope

        EnvelopesApi envelopesApi = new EnvelopesApi(apiClient);
        EnvelopeSummary results = envelopesApi.createEnvelope(accountId, envelopeDefinition);
        String envelopeId = results.getEnvelopeId();

        // Step 3. The envelope has been created.
        //         Request a Recipient View URL (the Signing Ceremony URL)
        RecipientViewRequest viewRequest = new RecipientViewRequest();
        // Set the url where you want the recipient to go once they are done signing
        // should typically be a callback route somewhere in your app.
        viewRequest.setReturnUrl(baseUrl);
        viewRequest.setAuthenticationMethod(authenticationMethod);
        viewRequest.setEmail(signerEmail);
        viewRequest.setUserName(signerName);
        viewRequest.setClientUserId(clientUserId);
        // call the CreateRecipientView API
        ViewUrl results1 = envelopesApi.createRecipientView(accountId, envelopeId, viewRequest);
        ApiResponse apiResponse = new ApiResponse(results1.getUrl());
        return apiResponse;
    }

    @PostMapping("update-status")
    public void updateStatus(@RequestParam(name = "event") String event) {
        System.out.println(event);
    }

}
