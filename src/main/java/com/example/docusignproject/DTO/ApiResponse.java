package com.example.docusignproject.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


public class ApiResponse {
    String url;

    public ApiResponse(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
